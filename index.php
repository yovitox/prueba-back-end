<html lang="es">
  <head>
  	<script src="lib/jquery.js" type="text/javascript"></script>
  	<meta charset="UTF-8">
  	<title> .::Prueba - Experis::. </title>
  </head>

<?php
 require ("app/class.changestring.php");
 require ("app/class.completerange.php");
 require ("app/class.clearpar.php");

    if( isset($_REQUEST["btnChangeString"]) && $_REQUEST["btnChangeString"] == "Cambiar" ){
      $changeString = new changeString();
      $changeString->build($_POST["txtChangeString"]);

    }elseif (isset($_REQUEST["txtComplete"]) && $_REQUEST["txtComplete"] == "Completar"){
			$completeRange = new completeRange();
      $range= [1, 2, 4, 5, 7];
      $completeRange->build($range);

		}elseif (isset($_REQUEST["btnClear"]) && $_REQUEST["btnClear"] == "Limpiar"){
			$clearPar = new ClearPar();
      $clearPar->build($_POST['txtPar']);
		}
?>
<body>
	<div id="contendor">
		<h1>Secuencia de Parametros</h1>
		<form action="" method="post">
			<input type="text" name="txtChangeString"  /> <br /> <br />
			<input type="submit" name="btnChangeString" id="idcs"  value="Cambiar" />
		</form>
	</div>


	<div id="contendor">
		<h1>Coleccion de Numeros</h1>

    <div id="lista">
			<ul id="lista-numeros"></ul>
		</div>
    <script type="text/javascript" language="javascript">
      $(document).on("ready", function(){

        var nro = 0;
        var listaNum = new Array();
        $("#agregar").on("click", function(){

          nro = $("#numero").val();
          // validando datos repetidos
          if((listaNum.indexOf(nro) == -1)){
            //agregando al html el valor del input
            $("#lista-numeros").append("<li>" + nro + "</li>");
            listaNum.push(nro);
          }else{
            alert("numero repetido");
          }
        });

    });
    document.frEnviar.hdnLisNum.value=listaNum;
    document.frEnviar.submit();

    </script>

		<form action="" method="post">
      <input type="number" id="numero" min="1" max="9" name="listnum" />

			<input type="button" name="Agregar" id="agregar" value="Agregar" /><br/><br/>
      <input type="submit" name="txtComplete" id="Complete" value="Completar" />

		</form>
	</div>

	<div id="contendor">
		<h1>Limpiar Par</h1>
		<form action="" method="post" name=frEnviar>
       <input type=hidden name=hdnLisNum>
			<input type="text" name="txtPar"  /> <br /> <br />
			<input type="submit" name="btnClear" id="clear"  value="Limpiar" />
		</form>
	</div>

</body>
</html>
