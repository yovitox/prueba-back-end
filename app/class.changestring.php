<?php
/******************************
 *  class.changestring.php    *
 ******************************/

class changeString {

  var $alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w' , 'x', 'y', 'z' ];

  public function build( $char ){

      $charSplit = str_split( $char );
      foreach ( $charSplit as $key => $value ) {
        $newChar = $this->newLetter( $char[$key] );
        if ( $newChar ) {
          $result = $newChar;
        }else{
          $result = $char[$key];
        }
        echo $result;
      }

  }

  // chack and return the letter plus one
  private function newLetter( $value ){

      $isUpper = ctype_upper( $value );
      if( $isUpper ) $value = strtolower( $value );
      $nletter = array_search( $value, $this->alphabet );
      if( $nletter !== false ){
        if( $nletter == 26 ) $nletter=-1;
        $value = $this->alphabet[$nletter+1];
        if( $isUpper ) $value = strtoupper( $value );
        return $value;
      }else{
        return false;
      }

  }

}
