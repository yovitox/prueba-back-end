<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../app/controllers/employeeController.php';
require '../functions/functions.php';

$app = new \Slim\App;

$container = $app->getContainer();

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../templates', [
        'cache' => '../templates/cache'
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    return $view;
};


$app->get('/api/v1/employees', function (Request $request, Response $response) {
	$employee = new EmployeeController();
	$result = $employee->getAll(); //  var_dump($result);

    return $this->view->render($response, '../templates/index.html', [
        'employees' => $result
    ]);
});


$app->get('/employees/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');
	$employee = new Employee();
	$result = $employee->getById($id);
    return $this->view->render($response, '../templates/detail.html', [
        'employee' => $result, 'id' => $id
    ]);
});


$app->get('/searchs/employee', function (Request $request, Response $response) {
	$email = $request->getParam('email');
	$employee = new Employee();
	$result = $employee->getByEmail($email);
    return $this->view->render($response, '../templates/search.html', [
        'employee' => $result
    ]);
});

$app->run();
